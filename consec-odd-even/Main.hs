{-# LANGUAGE TemplateHaskell #-}

module Main where
import Data.List ( tails )
import Control.Monad.State
    ( forM_, forM, when, modify, evalState, execState )
import Data.Char ( ord, chr )
import Control.Lens ( use, (??), (.=), (<>=), makeLenses )
import Data.Foldable ( forM_, traverse_ )

consecoddeven :: [Char] -> [Char]
consecoddeven s = [max x y | x:y:_ <- tails s, ord x `mod` 2 == ord y `mod` 2]

consecoddeven2 :: [Char] -> [Char]
consecoddeven2 s = execState (
        forM [1..(length s - 1)] (\i ->
            when (ord (s!!i) `mod` 2 == ord (s!!(i-1)) `mod` 2)
                $ modify (++[max (s!!i) (s!!(i-1))])
        )
    ) []

data S = S {_last_x :: Char, _s ::String}

initS :: S
initS = S (chr 0) ""

makeLenses ''S

consecoddeven3 :: [Char] -> [Char]
consecoddeven3 [] = ""
consecoddeven3 (x:xs) = evalState (do
        last_x .= x
        forM_ xs (\x -> do
            y <- use last_x
            when (ord x `mod` 2 == ord y `mod` 2)
                $ s <>= [max x y]
            last_x .= x
            )
        use s
    ) initS

main :: IO ()
-- main = print $ [consecoddeven, consecoddeven2, consecoddeven3] "1112031584"
main = traverse_ print $ [consecoddeven, consecoddeven2, consecoddeven3] ?? "1112031584"
