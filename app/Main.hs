{-#LANGUAGE FlexibleInstances, GADTs #-}

import Control.Monad.State ( forM_ )
import qualified Data.Map as M
import Data.Semigroup ( Sum )

isInt :: RealFrac a => a -> Bool
isInt n = n == fromInteger (round n)

data NularyOp = Four | FortyFour | PointFour   deriving (Eq, Ord, Enum)
data UnaryOp = Sqrt                            deriving (Eq, Ord, Enum)
data BinaryOp = Add | Sub | Mul | Div | Pow    deriving (Eq, Ord, Enum)

data Term n t where
    Nulary :: NularyOp -> Term n n
    Unary :: UnaryOp -> Term n (n -> n)
    Binary :: BinaryOp -> Term n (n -> n -> n)
    UnaryApply :: Term n (n -> n) -> Term n n -> Term n n
    BinaryApply :: Term n (n -> n -> n) -> Term n n -> Term n n -> Term n n


type Exp n = Term n n

class Enum t => Op t where
    ops :: [t]
    ops = [toEnum 0..]

instance Op NularyOp
instance Op UnaryOp
instance Op BinaryOp

lOpPrecedence :: BinaryOp -> Int
lOpPrecedence Add = 0
lOpPrecedence Sub = 0
lOpPrecedence Mul = 2
lOpPrecedence Div = 2
lOpPrecedence Pow = 5

rOpPrecedence :: BinaryOp -> Int
rOpPrecedence Add = 0
rOpPrecedence Sub = 1
rOpPrecedence Mul = 2
rOpPrecedence Div = 3
rOpPrecedence Pow = 4

lPrecedence :: Exp n -> Int
lPrecedence (Nulary _) = 7
lPrecedence (UnaryApply _ _) = 7
lPrecedence (BinaryApply (Binary op) _ _ ) = lOpPrecedence op

rPrecedence :: Exp n -> Int
rPrecedence (Nulary _) = 7
rPrecedence (UnaryApply _ _) = 7
rPrecedence (BinaryApply (Binary op) _ _) = rOpPrecedence op

instance Show NularyOp where
    show Four = "4"
    show FortyFour = "44"
    show PointFour = ".4"

instance Show UnaryOp where
    show Sqrt = "sqrt"

instance Show BinaryOp where
    show Add = " + "
    show Sub = " - "
    show Mul = " x "
    show Div = " / "
    show Pow = "^"

instance Show (Exp n) where
    show (Nulary op) = show op
    show (UnaryApply (Unary op) e) = show op ++ "(" ++ show e ++ ")"
    show (BinaryApply (Binary op) e1 e2) = let wrap b e = if b then "(" ++ e ++ ")" else e
                                               e1w = wrap (lOpPrecedence op > rPrecedence e1) (show e1)
                                               e2w = wrap (rOpPrecedence op > lPrecedence e2) (show e2)
                                           in e1w ++ show op ++ e2w


eval :: Floating n => Term n t -> t
eval (Nulary Four) = 4
eval (Nulary FortyFour) = 44
eval (Nulary PointFour) = 0.4

eval (Unary Sqrt) = sqrt

eval (Binary Add) = (+)
eval (Binary Sub) = (-)
eval (Binary Mul) = (*)
eval (Binary Div) = (/)
eval (Binary Pow) = (**)

eval (UnaryApply f x) = eval f (eval x)
eval (BinaryApply f x y) = eval f (eval x) (eval y)


fourCount :: Term n t -> Sum Int
fourCount (Nulary Four) = 1
fourCount (Nulary FortyFour) = 2
fourCount (Nulary PointFour) = 1

fourCount (Unary _) = 0

fourCount (Binary _) = 0

fourCount (UnaryApply f x) = fourCount x
fourCount (BinaryApply f x y) = fourCount x + fourCount y

complexity :: Term n t -> Sum Int
complexity (Nulary Four) = 0
complexity (Nulary FortyFour) = 1
complexity (Nulary PointFour) = 7

complexity (Unary Sqrt) = 8

complexity (Binary Add) = 2
complexity (Binary Sub) = 3
complexity (Binary Mul) = 4
complexity (Binary Div) = 5
complexity (Binary Pow) = 6

complexity (UnaryApply f x) = maximum [complexity f, complexity x]
complexity (BinaryApply f x y) = maximum [complexity f, complexity x, complexity y]


opCount :: Term n t -> Sum Int
opCount (Nulary _) = 1
opCount (Unary _) = 1
opCount (Binary _) = 1

opCount (UnaryApply f x) = opCount f + opCount x
opCount (BinaryApply f x y) = opCount f + opCount x + opCount y


simplest :: Exp n -> Exp n -> Exp n
simplest n o = if complexity n < complexity o
               then n
               else o

strikeUnlessSimpler :: Exp n -> Exp n -> Maybe (Exp n)
strikeUnlessSimpler n o = if complexity n < complexity o
                          then Just n
                          else Nothing

splitOn :: Ord k => (a -> k) -> [a] -> M.Map k [a]
splitOn f xs = M.fromListWith mappend [(f x, [x]) | x <- xs]

seeds :: [Exp n]
seeds = [Nulary op | op <- ops]

apply1 :: [Exp n] -> [Exp n]
apply1 as = [UnaryApply (Unary op) a | op <- ops, a <- as]

apply2 :: [Exp n] -> [Exp n] -> [Exp n]
apply2 as bs = [BinaryApply (Binary op) a b | op <- ops, a <- as, b <- bs]

apply1restrict :: Ord k => (k -> Bool) -> M.Map k [Exp n] -> [Exp n]
apply1restrict p am = mconcat [apply1 (am M.! ak) | ak <- M.keys am, p ak]

apply2restrict :: (Semigroup k, Ord k) => (k -> Bool) -> M.Map k [Exp n] -> M.Map k [Exp n] -> [Exp n]
apply2restrict p am bm = mconcat [apply2 (am M.! ak) (bm M.! bk) | ak <- M.keys am, bk <- M.keys bm, p (ak <> bk)]

extend :: [Exp n] -> [Exp n] -> [Exp n]
extend old new = let classify e = (fourCount e, opCount e)
                     oldmap = splitOn classify old
                     newmap = splitOn classify new
                     pred (fc, oc) = fc <= 4 && oc <= 7
                 in apply2restrict pred oldmap newmap ++ apply2restrict pred newmap oldmap ++ apply2restrict pred newmap newmap ++ apply1restrict pred newmap

-- Store expressions in a map indexed by number of fours used and value,
-- so as to keep only the simplest for each case. Don't store if number of
-- fours is greater than four
refine :: RealFloat n => [Exp n] -> M.Map (Sum Int, n) (Exp n)
refine es = M.fromListWith simplest [((fc, val), e) | e <-es, let fc = fourCount e, fc <= 4, let val = eval e, not (isNaN val || isInfinite val || isDenormalized val)]

exps :: RealFloat n => M.Map (Sum Int, n) (Exp n)
exps = let infer old new = if M.null new
                           then old
                           else let
                                    discovered = refine $ extend (M.elems old) (M.elems new)
                                    old' = M.unionWith simplest old new
                                    new' = M.differenceWith strikeUnlessSimpler discovered old'
                                in infer old' new'
       in infer M.empty $ refine seeds

main :: IO ()
main = forM_ ([(v, M.lookup (4, v) exps) | v <- [1..200]])
            (\(val,exp) -> putStrLn $ show val ++ " -> " ++ maybe "##################" show exp)
