{-# LANGUAGE TemplateHaskell #-}
module Main where

import Control.Lens ( makeLenses, (-=), use, (.=) )
import Control.Monad.State.Lazy ( evalState )
import Control.Monad.Loops ( whileM )
import Control.Monad.Extra ( ifM )
import Control.Applicative ( Applicative(liftA2) )
import Data.Foldable


data GCDState = GCDState{_x :: Int,
                         _y :: Int}

makeLenses ''GCDState

initialState :: GCDState
initialState = GCDState 0 0

(./=.) :: Eq a => Applicative f => f a -> f a -> f Bool
(./=.) = liftA2 (/=)

(.>.) :: Ord a => Applicative f => f a -> f a -> f Bool
(.>.) = liftA2 (>)

mgcd :: Int -> Int -> Int
mgcd a b = evalState
       ( do
              x .= a
              y .= b
              whileM (use x ./=. use y)
                     (ifM (use x .>. use y)
                            ((x -=) =<< use y)
                            ((y -=) =<< use x))
              use x
       ) initialState

main = traverse_ print $ uncurry mgcd <$> [(12,16), (96,156),(240,330)]