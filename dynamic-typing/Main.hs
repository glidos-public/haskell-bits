{-#LANGUAGE GADTs
          , DataKinds
          , TypeFamilies
#-}

module Main where

import Data.Typeable ( Typeable, cast )
import Control.Monad ( (<=<) )


data NatT = ZeroT | SuccT NatT

type OneT = SuccT ZeroT
type TwoT = SuccT OneT

data TypedOp (n :: NatT) where
    Four :: TypedOp ZeroT
    FortyFour :: TypedOp ZeroT
    PointFour :: TypedOp ZeroT
    Sqrt :: TypedOp OneT
    Add :: TypedOp TwoT
    Sub :: TypedOp TwoT
    Mul :: TypedOp TwoT
    Div :: TypedOp TwoT
    Pow :: TypedOp TwoT

data Op where
    Op :: Typeable n => TypedOp n -> Op

data LooseTerm where
    LooseOpTerm :: Op -> LooseTerm
    LooseApply :: LooseTerm -> [LooseTerm] -> LooseTerm

type family FuncN n where
    FuncN ZeroT = Double
    FuncN (SuccT n) = Double -> FuncN n

data ListN n a where
    EmptyN :: ListN ZeroT a
    ConsN :: a -> ListN n a -> ListN (SuccT n) a

instance Functor (ListN n) where
    fmap f EmptyN = EmptyN
    fmap f (ConsN a as) = ConsN (f a) (fmap f as)

data SomeListN a where
    SomeListN :: Typeable n => ListN n a -> SomeListN a

apply :: FuncN n -> ListN n Double -> Double
apply x EmptyN = x
apply f (ConsN x xs) = apply (f x) xs


data Term n where
    OpTerm :: TypedOp n -> Term n
    Apply :: Typeable n => Term n -> ListN n (Term ZeroT) -> Term ZeroT

data SomeTerm where
    SomeTerm :: Typeable n => Term n -> SomeTerm


evalOp :: TypedOp n -> FuncN n
evalOp Four = 4
evalOp FortyFour = 44
evalOp PointFour = 0.4
evalOp Sqrt = sqrt
evalOp Add = (+)
evalOp Sub = (-)
evalOp Mul = (*)
evalOp Div = (/)
evalOp Pow = (**)


list2ListN :: [a] -> SomeListN a
list2ListN [] = SomeListN EmptyN
list2ListN (x:xs) = case list2ListN xs of SomeListN ls -> SomeListN $ ConsN x ls

checknullary :: SomeTerm -> Maybe (Term ZeroT)
checknullary (SomeTerm t) = cast t

typecheck :: LooseTerm -> Maybe SomeTerm
typecheck (LooseOpTerm o) = case o of Op op -> Just $ SomeTerm $ OpTerm op
typecheck (LooseApply l ls) = do SomeTerm tn <- typecheck l
                                 SomeListN tns <- list2ListN <$> mapM (checknullary <=< typecheck) ls
                                 SomeTerm <$> (Apply <$> cast tn <*> Just tns)

eval :: Term n -> FuncN n
eval (OpTerm o) = evalOp o
eval (Apply o ts) = apply (eval o) (fmap eval ts)

main :: IO ()
main = let tt = (checknullary <=< typecheck) $ LooseApply (LooseOpTerm $ Op Add) [LooseOpTerm $ Op Four, LooseOpTerm $ Op Four]
       in case tt of Just t -> print $ eval t
                     Nothing -> print "Failed"
