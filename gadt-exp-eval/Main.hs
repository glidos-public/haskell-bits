{-#LANGUAGE FlexibleInstances, GADTs, DataKinds, TypeFamilies #-}

module Main where

data NatT = ZeroT | SuccT NatT

type OneT = SuccT ZeroT
type TwoT = SuccT OneT

type family FuncN n where
    FuncN ZeroT = Double
    FuncN (SuccT n) = Double -> FuncN n

data ListN n a where
    EmptyN :: ListN ZeroT a
    ConsN :: a -> ListN n a -> ListN (SuccT n) a

instance Functor (ListN n) where
    fmap f EmptyN = EmptyN
    fmap f (ConsN a as) = ConsN (f a) (fmap f as)

apply :: FuncN n -> ListN n Double -> Double
apply x EmptyN = x
apply f (ConsN x xs) = apply (f x) xs

data NularyOp = Four | FortyFour | PointFour   deriving (Eq, Ord, Enum)
data UnaryOp = Sqrt                            deriving (Eq, Ord, Enum)
data BinaryOp = Add | Sub | Mul | Div | Pow    deriving (Eq, Ord, Enum)

class Op o where
    type Arity o :: NatT
    evalOp :: o -> FuncN (Arity o)

instance Op NularyOp where
    type Arity NularyOp = ZeroT
    evalOp Four = 4
    evalOp FortyFour = 44
    evalOp PointFour = 0.4

instance Op UnaryOp where
    type Arity UnaryOp = OneT
    evalOp Sqrt = sqrt

instance Op BinaryOp where
    type Arity BinaryOp = TwoT
    evalOp Add = (+)
    evalOp Sub = (-)
    evalOp Mul = (*)
    evalOp Div = (/)
    evalOp Pow = (**)

data Term n where
    OpTerm :: Op o => o -> Term (Arity o)
    Apply :: Term n -> ListN n (Term ZeroT) -> Term ZeroT


eval :: Term n -> FuncN n
eval (OpTerm o) = evalOp o
eval (Apply o ts) = apply (eval o) (fmap eval ts)

main :: IO ()
main = print $ eval (OpTerm Four)
