{-# LANGUAGE LambdaCase#-}
module Main where

import Control.Monad ( forM_ )

maxRevenue:: (Int -> Int) -> Int -> Int
maxRevenue price n = maximum (price n:[maxRevenue price y + maxRevenue price (n-y) | y <- [1..n-1]])




optimise:: (Int -> Int) -> Int -> (Int -> Int)
optimise price 1 = price
optimise price n = let optimised = optimise price (n-1) in
                   let nval = maximum (price n:[optimised y + optimised (n-y) | y <- [1..n-1]]) in
                   (\x -> if x == n then nval
                                    else optimised x)

maxRevenueOpt:: (Int -> Int) -> Int -> Int
maxRevenueOpt price len = optimise price len len




maxRevenueOpt2:: (Int -> Int) -> Int -> Int
maxRevenueOpt2 price = maxRev
    where maxRev n = maximum (price n:[maxRevMem y + maxRevMem (n-y) | y <- [1..n-1]])
          maxRevMem = (map maxRev [0..] !!)

main :: IO ()
main = do
    forM_ tests (print . runTest)


data Test = Test (Int -> Int) Int Int

runTest:: Test -> (Int, Int)
runTest (Test price len result) = (maxRevenueOpt2 price len, result)

tests :: [Test]
tests =
    [
        Test (\case 1 -> 10
                    _ -> 0)
        1
        10,

        Test (\case 1 -> 10
                    2 -> 25
                    _ -> 0)
        2
        25,

        Test (\case 1 -> 10
                    2 -> 19
                    _ -> 0)
        2
        20,

        Test (\case 1 -> 10
                    2 -> 25
                    3 -> 30
                    _ -> 0)
        3
        35,

        Test (\case 1 -> 1
                    2 -> 7
                    3 -> 9
                    4 -> 14
                    5 -> 14
                    _ -> 0)
        5
        16,

        Test (\x -> if x == 49 then 50
                               else x)
        100
        102
    ]